


DELETE edge E;
DELETE vertex V;



BEGIN;


/* Aufgaben */

let aufgabe1 = CREATE vertex Aufgabe set 
    Name = "Kontomanager", Beschreibung = "Hält das Vereinskonto, überprüft Einnahmen und Ausgaben";
let aufgabe2 = CREATE vertex Aufgabe set 
    Name = "Ansprechperson", Beschreibung = "Ansprechperson für Presse und Co.";
let aufgabe3 = CREATE vertex Aufgabe set 
    Name = "Finanzmanager", Beschreibung = "Überprüft Finanzen, schaut dass Ausgaben des Vereins nicht die Einahmen übertreffen";
let aufgabe4 = CREATE vertex Aufgabe set 
    Name = "Eventorganisation", Beschreibung = "Organisiert Vereinsveranstaltungen und verteilt Betreuer";

commit;
/* Mitglieder */

let mitglied1 = CREATE vertex Person set 
    Name = {"Vorname":"Annika","Nachname":"Schmid"}, 
    Email = "annika.schmid@verein.de", 
    TelefonNr = ["+49 75122321","+49 751/321432"], 
    Beitrittsdatum = "2010-06-02", 
    Adresse = {"Hausnummer":32,"PLZ":88250,"Stadt":"Weingarten","Strasse":"Weingartener Weg"},
    Typ = "mitglied",
    Mitgliedstyp = "Standartmitglied",
    Mitgliedsnummer = 2134;

let mitglied2 = CREATE vertex Person set 
    Name = {"Vorname":"Peter","Nachname":"Maier"}, 
    Email = "p.maier@verein.de", 
    TelefonNr = ["+49 751/321","+49 751/32684"], 
    Beitrittsdatum = "2005-01-08", 
    Adresse = {"Hausnummer":42,"PLZ":88250,"Stadt":"Weingarten","Strasse":"Weinweg"},
    Typ = "mitglied",
    Mitgliedstyp = "Standartmitglied",
    Mitgliedsnummer = 8795;

let mitglied3 = CREATE vertex Person set 
    Name = {"Vorname":"Maria","Nachname":"Bank"}, 
    Email = "maria.db@verein.de", 
    TelefonNr = ["+49 751/5429"], 
    Beitrittsdatum = "2009-11-28", 
    Adresse = {"Hausnummer":10,"PLZ":88250,"Stadt":"Weingarten","Strasse":"Dieselstraße"},
    Typ = "mitglied",
    Mitgliedstyp = "Standartmitglied",
    Mitgliedsnummer = 8195;

let mitglied4 = CREATE vertex Person set 
    Name = {"Vorname":"Max","Nachname":"Mustermann"}, 
    Email = "max.mustermann@verein.de", 
    TelefonNr = ["+49 123/456"], 
    Beitrittsdatum = "2000-10-18", 
    Adresse = {"Hausnummer":10,"PLZ":12345,"Stadt":"Musterstadt","Strasse":"Musterweg"},
    Typ = "mitglied",
    Mitgliedstyp = "Vorstand",
    Mitgliedsnummer = 1337;

commit;
/* Öffentliche Personen */

let oeffentlPerson1 = CREATE vertex Person set 
    Name = {"Vorname":"Anna","Nachname":"Bauer"}, 
    Email = "anna.bauer@musterstadt.de", 
    TelefonNr = ["+49 123/456789"], 
    Adresse = {"Hausnummer":101,"PLZ":12345,"Stadt":"Musterstadt","Strasse":"Musterallee"},
    Typ = "funktionsträger",
    Funktion = "Bürgermeister",
    Dienststelle = "Bürgermeisteramt Musterstadt";

let oeffentlPersonUndMitglied = CREATE vertex Person set 
    Name = {"Vorname":"Rüdiger","Nachname":"Weber"}, 
    Email = "anna.bauer@musterstadt.de", 
    TelefonNr = ["+49 123/45672456"], 
    Adresse = {"Hausnummer":1,"PLZ":51234,"Stadt":"Musterdorf","Strasse":"Bürgermeisterweg"},
    Typ = "mitglied_und_funktionsträger",
    Funktion = "Bürgermeister",
    Beitrittsdatum = "2014-05-2",
    Dienststelle = "Bürgermeisteramt Musterdorf",
    Mitgliedstyp = "Standartmitglied",
    Mitgliedsnummer = 420;

commit;

/* Radtouren */
let radtour1 = CREATE vertex Veranstaltung set 
    Name = "Radtour Bodensee",
    Beschreibung = "Eine Radtour um den Bodensee, beginnend in RV am Hbf",
    Schwierigkeit = "mittel",
    Radempfehlung = "Trekkingrad",
    Distanz = 78,
    Dauer = 1200,
    Typ = "radtour",
    Zeitpunkt = "2019-02-11 12:30:00",
    Treffpunkt = {
        "Bezeichnung":"Hauptbahnhof Ravensburg",
        "Breitengrad":47.784094,
        "Laengengrad":9.605965
    };


let radtour2 = CREATE vertex Veranstaltung set
    Name = "Radtour Weingarten",
    Beschreibung = "Eine Radtour rund um Weingarten",
    Schwierigkeit = "leicht",
    Radempfehlung = "Trekking- oder Cityrad",
    Distanz = 10,
    Dauer = 60,
    Zeitpunkt = "2019-03-12 12:00:00",
    Typ = "radtour",
    Treffpunkt = {
        "Bezeichnung":"Basilika",
        "Breitengrad":47.808858,
        "Laengengrad":9.642985
    };

/* kurse */
let kurs1 = CREATE vertex Veranstaltung set 
    Name = "Sicheres Fahren in der Stadt",
    Zeitpunkt = "2015-08-11 12:30:00",
    Beschreibung = "Ein Kurs, bei welchem das sichere Verhalten in der Stadt beigebracht wird",
    Typ = "kurs",
    Treffpunkt = {
        "Bezeichnung":"Stadtpark",
        "Breitengrad":47.807533,
        "Laengengrad":9.638519
    },
    Lernziele = [
        "Handhabung Fahrrad",
        "Verkehrssicherheit",
        "Rücksichtsvolles Teilnehmen am Verkehr"
    ],
    Teilnahmegebuer = 5.30;

/* gleichzeitig kurs und radtour*/

let kursUndRadtour = CREATE vertex Veranstaltung set 
    Name = "Sicheres Radfahren - Praktikum",
    Beschreibung = "Auf einer abgesicherten Strecke wird das Radfahren erlernt",
    Typ = "radtour_und_kurs",
    Radempfehlung = "Beliebiges Rad",
    Distanz = 5,
    Dauer = 40,
    Schwierigkeit = "sehr leicht",
    Treffpunkt = {
        "Bezeichnung":"Gymnasium Weingarten",
        "Breitengrad":47.811169,
        "Laengengrad":9.629013
    },
    Lernziele = [
        "Verbesserung der Radfahrfähigkeiten"
    ],
    Zeitpunkt = "2015-05-10 11:30:00",
    Teilnahmegebuer = 14.99;

/* generische veranstaltung */

let veranstaltung1 = CREATE vertex Veranstaltung set 
    Name = "Mitgliedstreffen",
    Beschreibung = "Mitgliedstreffen des Vereins",
    Zeitpunkt = "2019-12-11 18:45:00",
    Typ = "veranstaltung",
    Treffpunkt = {
        "Bezeichnung":"Vereinshaus",
        "Breitengrad":47.651723,
        "Laengengrad":9.478990
    };
commit;

/*
 *
 *  KANTEN
 *
 *
 */

let uebernimmt1 = CREATE edge uebernimmt from $mitglied1 to $aufgabe1 set
    Anfangsdatum = "2018-02-01",
    Enddatum = null;
let uebernimmt2 = CREATE edge uebernimmt from $mitglied2 to $aufgabe2 set 
    Anfangsdatum = "2006-01-01",
    Enddatum = "2019-05-05";
let uebernimmt3 = CREATE edge uebernimmt from $mitglied3 to $aufgabe2 set 
    Anfangsdatum = "2019-05-06",
    Enddatum = null;
let uebernimmt4 = CREATE edge uebernimmt from $mitglied4 to $aufgabe3 set 
    Anfangsdatum = "2014-06-01",
    Enddatum = "2019-01-04";
let uebernimmt5 = CREATE edge uebernimmt from $mitglied4 to $aufgabe4 set 
    Anfangsdatum = "2019-01-05",
    Enddatum = null;

let betreut1 = CREATE edge betreut from $mitglied1 to $kurs1;
let betreut2 = CREATE edge betreut from $mitglied2 to $kurs1;
let betreut3 = CREATE edge betreut from $mitglied1 to $kursUndRadtour; 
let betreut4 = CREATE edge betreut from $mitglied3 to $radtour1; 
let betreut5 = CREATE edge betreut from $mitglied4 to $radtour2;


commit;

END;