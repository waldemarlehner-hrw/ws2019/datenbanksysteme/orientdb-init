/*
SET echo true;
SET ignoreErrors true;

*/
DELETE edge E;
DELETE vertex V;



DROP class Person           IF EXISTS;
DROP class Aufgabe          IF EXISTS;
DROP class Veranstaltung    IF EXISTS;

DROP class betreut          IF EXISTS;
DROP class uebernimmt       IF EXISTS;

DROP class Namen            IF EXISTS;
DROP class Treffpunkt       IF EXISTS;
DROP class Adresse          IF EXISTS;

CREATE class Adresse;
CREATE class Namen;
CREATE class Person extends V;

CREATE class Veranstaltung extends V;

CREATE class Aufgabe extends V;
CREATE class Treffpunkt;

CREATE class uebernimmt extends E;
CREATE class betreut extends E;
/* PERSON */
CREATE property Namen.Vorname string                            (mandatory,notnull);
CREATE property Namen.Nachname string                           (mandatory,notnull);

CREATE property Adresse.PLZ integer                             (mandatory,notnull,min 0,max 99999);
CREATE property Adresse.Stadt string                            (mandatory,notnull);
CREATE property Adresse.Strasse string                          (mandatory,notnull);
CREATE property Adresse.Hausnummer integer                      (mandatory,notnull, min 1);
CREATE property Person.Typ string                               (mandatory,notnull);
 ALTER property Person.Typ regexp "mitglied|person|funktionsträger|mitglied_und_funktionsträger";
CREATE property Person.Name embedded Namen                      (mandatory,notnull);
CREATE property Person.Email string                             (mandatory);
CREATE property Person.TelefonNr embeddedlist string            (mandatory,notnull);
CREATE property Person.Adresse embedded Adresse                 (mandatory,notnull);
/* ENDE PERSON */

/* !!! Da in OrientDB overlapping Vererbungen nicht möglich sind werden die Mitglieds- und 
       "Person der Öffentlichkeit"-Klassen direkt in der Person gespeichert. Durch das Enum 
       "Typ" lässt sich Bestimmen ob es sich um ein Mitglied, eine Person d. Öffentlichkeit oder 
       um beides Handelt  
*/

/* MITGLIED */
CREATE property Person.Mitgliedstyp string                      (notnull);
CREATE property Person.Mitgliedsnummer integer                  (notnull, min 0);
CREATE property Person.Beitrittsdatum date                      (notnull,readonly);
/* ENDE MITGLIED */
/* FUNKTIONSTRÄGER */
CREATE property Person.Funktion string                          (notnull);

CREATE property Person.Dienststelle string                      (notnull);
/* ENDE FUNKTIONSTRÄGER */

/* VERANSTALTUNG */
CREATE property Treffpunkt.Bezeichnung string                   (mandatory);
CREATE property Treffpunkt.Breitengrad double                   (mandatory,notnull, min  -90, max  90) ;
CREATE property Treffpunkt.Laengengrad double                   (mandatory,notnull, min -180, max 180) ;
CREATE property Veranstaltung.Name string                       (mandatory,notnull);
CREATE property Veranstaltung.Treffpunkt embedded Treffpunkt    (mandatory);
CREATE property Veranstaltung.Zeitpunkt datetime                (mandatory, notnull);

CREATE property Veranstaltung.Typ string                        (mandatory, notnull);
ALTER property Veranstaltung.Typ regexp "veranstaltung|radtour|kurs|radtour_und_kurs";

/* !!! Da in OrientDB overlapping Vererbungen nicht möglich sind werden die Kurs- und 
       Veranstaltung-Klassen direkt in der Person gespeichert. Durch das Enum 
       "Typ" lässt sich Bestimmen ob es sich um eine Radtour, einen Kurs oder 
       um beides Handelt  
*/

/* END VERANSTALTUNG */

/* KURS */
CREATE property Veranstaltung.Lernziele embeddedlist string   ; 
    
CREATE property Veranstaltung.Teilnahmegebuer decimal           (notnull, min 0);
/* END KURS */
/* RADTOUR */
CREATE property Veranstaltung.Dauer integer /* in minuten*/     (notnull, min 0);

CREATE property Veranstaltung.Distanz double                    (notnull, min 0);


CREATE property Veranstaltung.Radempfehlung string              (notnull);

CREATE property Veranstaltung.Schwierigkeit string              (notnull, regexp "sehr leicht|leicht|mittel|schwer|sehr schwer");

/*ENDE RADTOUR*/


/* KANTEN */
/* ###### */

CREATE property uebernimmt.Anfangsdatum date                    (mandatory,notnull,readonly);

CREATE property uebernimmt.Enddatum date                        (mandatory);                 
/*
CREATE property betreut.Zeitpunkt datetime                      (mandatory,notnull);
*/