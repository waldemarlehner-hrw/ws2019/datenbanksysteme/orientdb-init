/*
*   Aufgabe 3.1
*   Daten einer Vertex-Klasse abfragen
*   
*   Es werden alle Properties aller Radtouren mit einer Distanz < 50km ausgegeben
*/

select * from Veranstaltung where Typ = "radtour" and Distanz < 50;

/*
*   Aufgabe 3.2
*   Daten von  3 Vertex-Klasse und 2 Edge-Klassen abfragen
*   
*   Es werden alle Mitglieder mit Vor- und Nachname ausgegeben, sowie deren Aufgaben und betreute
*   Veranstaltungen, welche mindestens eine Veranstaltung  und mindestens eine Aufgabe übernehmen 
*   oder übernommen haben.
*   [!!!] Nicht als Command, sondern als Batch einlesen, da der Befehl über mehrere Zeilen geht.
*/

select
    outE("uebernimmt"),
    out("betreut") as Veranstaltungen,
    out("uebernimmt") as Aufgaben,
    out("betreut").size()+out("uebernimmt").size() as Gesamtanzahl,

    Name.Vorname + " " + Name.Nachname as Name
from 
    Person
where 
    (Typ = "mitglied" or Typ = "mitglied_und_funktionsträger") /* Nur Mitglieder */
    and
    (out("betreut").size() > 0 and out("uebernimmt").size() > 0) /*  welche Aufgaben übernommen und Kurse begleitet haben */

order by 
    Gesamtanzahl
desc;

/*
*   Aufgabe 3.3
*   Traverse Abfrage
*
*/

select * from (traverse out() from Person)

